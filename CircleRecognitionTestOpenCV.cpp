﻿// CircleRecognitionTestOpenCV.cpp : Defines the entry point for the console application.
//
#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\highgui\highgui_c.h>
#include <opencv2\imgproc\imgproc.hpp>

#include <opencv2\video\video.hpp>

#include <vector>


#define ESCAPE_KEY 27


using std::cin;
using std::cout;
using std::vector;


static const char* WebCamWindowName = "WebcamOutput";


void DrawCircles(cv::Mat &mat, vector<cv::Vec3f> &circles);


int _tmain(int argc, _TCHAR* argv[])
{
	std::locale::global(std::locale("German_germany"));

	cv::namedWindow(WebCamWindowName, CV_WINDOW_AUTOSIZE);

	cv::VideoCapture camera;
	camera.open(0);

	if (!camera.isOpened())
	{
		std::cerr << "ERROR: Couldn't access camera." << std::endl;
		exit(-1);
	}

	camera.set(CV_CAP_PROP_FRAME_WIDTH, 640);
	camera.set(CV_CAP_PROP_FRAME_HEIGHT, 480);

	vector<cv::Vec3f> circles;
	cv::Mat cameraFrame, frameGray;

	while(1)
	{
		camera >> cameraFrame;
		if (cameraFrame.empty()) {
			std::cerr << "ERROR: Couldn't grab image." << std::endl;
			exit(1);
		}

		cv::cvtColor(cameraFrame, frameGray, CV_RGB2GRAY);
		cv::GaussianBlur(frameGray, frameGray, cv::Size(9, 9), 2, 2);
		cv::HoughCircles(frameGray, circles, CV_HOUGH_GRADIENT, 1.3, frameGray.rows / 16, 200);

		/// Draw the detected circles 
		DrawCircles(cameraFrame, circles);

		cv::imshow(WebCamWindowName, cameraFrame);

		char keyPress = cv::waitKey(20);
		if (keyPress == ESCAPE_KEY) { break; }
	}

	return 1;
} // _tmain


void DrawCircles(cv::Mat &mat, vector<cv::Vec3f> &circles)
{
	for(size_t i = 0; i < circles.size(); i++ ) {
		cv::Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
		int radius = cvRound(circles[i][2]);

		// circle center
		circle(mat, center, 3, cv::Scalar(0,255,0), -1, 8, 0 );

		// circle outline
		circle(mat, center, radius, cv::Scalar(0,0,255), 3, 8, 0 );
	}
} // DrawCircles